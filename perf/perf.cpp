#include <opencv2/core/core.hpp>
#include <opencv2/ts/ts_perf.hpp>
#include "spherical_mirror.h"

using namespace std;
using namespace cv;

struct EngineHandler
{
    sphermir_engine_t* engine;

    EngineHandler();
    ~EngineHandler();
};

EngineHandler::EngineHandler() : engine(0)
{
    sphermir_error_t err = sphermir_create(&engine);
    CV_Assert(err == SPHERMIR_SUCCESS);
}

EngineHandler::~EngineHandler()
{
    if (engine)
        sphermir_release(engine);
}

void genDefaultParams(sphermir_parameters_t& params)
{
    params.output_width = 1920;
    params.output_height = 1080;
    params.output_interpolation = SPHERMIR_INTER_LINEAR;

    params.dome_radius = 2.5;
    params.dome_angle = 0.0;

    params.mirror_position[0] = 2.5;
    params.mirror_position[1] = 0.0;
    params.mirror_position[2] = -0.2;
    params.mirror_radius = 0.6;

    params.projector_position[0] = 2.0;
    params.projector_position[1] = 0.0;
    params.projector_position[2] = -0.2;
    params.projector_euler[0] = params.projector_euler[1] = params.projector_euler[2] = 0.0;
    params.projector_throw_ratio = 1.6;

    params.source_scale = 1.0;
    params.source_angle = 0.0;
    params.source_mirrored = 0;
}

PERF_TEST(CalcMaps, CalcMaps)
{
    sphermir_error_t err;

    EngineHandler h;

    sphermir_parameters_t params;
    genDefaultParams(params);

    err = sphermir_set_params(h.engine, &params);
    ASSERT_EQ(err, SPHERMIR_SUCCESS);

    TEST_CYCLE()
    {
        sphermir_set_params(h.engine, &params);
    }

    SANITY_CHECK(0);
}

PERF_TEST(Convert_8UC1, Convert_8UC1)
{
    sphermir_error_t err;

    EngineHandler h;

    sphermir_parameters_t params;
    genDefaultParams(params);

    err = sphermir_set_params(h.engine, &params);
    ASSERT_EQ(err, SPHERMIR_SUCCESS);

    Mat src(4096, 4096, CV_8UC1);
    Mat dst(params.output_height, params.output_width, CV_8UC1);

    declare.in(src, dst, WARMUP_RNG);

    err = sphermir_convert_8uc1(h.engine, src.data, src.rows, src.cols, src.step, dst.data, dst.rows, dst.cols, dst.step);
    ASSERT_EQ(err, SPHERMIR_SUCCESS);

    TEST_CYCLE()
    {
        sphermir_convert_8uc1(h.engine, src.data, src.rows, src.cols, src.step, dst.data, dst.rows, dst.cols, dst.step);
    }

    SANITY_CHECK(dst);
}

int main(int argc, char* argv[])
{
    ::perf::Regression::Init("SphericalMirror");
    ::perf::TestBase::Init(argc, argv);
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
