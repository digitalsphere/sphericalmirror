#include <opencv2/imgproc/imgproc.hpp>
#include "engine.h"

using namespace std;
using namespace cv;

#define assertReturn(expr, code) if (!(expr)) return (code)

sphermir_engine_t::sphermir_engine_t()
{
    params_.output_width = 1024;
    params_.output_height = 768;
    params_.output_interpolation = SPHERMIR_INTER_LINEAR;

    params_.dome_radius = 5.0;
    params_.dome_angle = 0.0;

    params_.mirror_position[0] = 3.0;
    params_.mirror_position[1] = 0.0;
    params_.mirror_position[2] = 0.0;
    params_.mirror_radius = 0.6;

    params_.projector_position[0] = 2.0;
    params_.projector_position[1] = 0.0;
    params_.projector_position[2] = 0.0;
    params_.projector_euler[0] = params_.projector_euler[1] = params_.projector_euler[2] = 0.0;
    params_.projector_throw_ratio = 0.6;

    params_.source_scale = 1.0;
    params_.source_angle = 0.0;
    params_.source_mirrored = 0;

    calc_maps();
}

sphermir_error_t sphermir_engine_t::load_params(const char* file_name)
{
    FileStorage file(file_name, FileStorage::READ);
    assertReturn(file.isOpened(), SPHERMIR_FILE_OPEN_ERROR);

    FileNode output_width_node = file["output_width"];
    assertReturn(!output_width_node.empty(), SPHERMIR_FILE_READ_ERROR);
    assertReturn(output_width_node.isInt(), SPHERMIR_FILE_READ_ERROR);

    FileNode output_height_node = file["output_height"];
    assertReturn(!output_height_node.empty(), SPHERMIR_FILE_READ_ERROR);
    assertReturn(output_height_node.isInt(), SPHERMIR_FILE_READ_ERROR);

    FileNode output_interpolation_node = file["output_interpolation"];
    assertReturn(!output_interpolation_node.empty(), SPHERMIR_FILE_READ_ERROR);
    assertReturn(output_interpolation_node.isInt(), SPHERMIR_FILE_READ_ERROR);

    FileNode dome_radius_node = file["dome_radius"];
    assertReturn(!dome_radius_node.empty(), SPHERMIR_FILE_READ_ERROR);
    assertReturn(dome_radius_node.isReal() || dome_radius_node.isInt(), SPHERMIR_FILE_READ_ERROR);

    FileNode dome_angle_node = file["dome_angle"];
    assertReturn(!dome_angle_node.empty(), SPHERMIR_FILE_READ_ERROR);
    assertReturn(dome_angle_node.isReal() || dome_angle_node.isInt(), SPHERMIR_FILE_READ_ERROR);

    FileNode mirror_position_x_node = file["mirror_position_x"];
    assertReturn(!mirror_position_x_node.empty(), SPHERMIR_FILE_READ_ERROR);
    assertReturn(mirror_position_x_node.isReal() || mirror_position_x_node.isInt(), SPHERMIR_FILE_READ_ERROR);

    FileNode mirror_position_y_node = file["mirror_position_y"];
    assertReturn(!mirror_position_y_node.empty(), SPHERMIR_FILE_READ_ERROR);
    assertReturn(mirror_position_y_node.isReal() || mirror_position_y_node.isInt(), SPHERMIR_FILE_READ_ERROR);

    FileNode mirror_position_z_node = file["mirror_position_z"];
    assertReturn(!mirror_position_z_node.empty(), SPHERMIR_FILE_READ_ERROR);
    assertReturn(mirror_position_z_node.isReal() || mirror_position_z_node.isInt(), SPHERMIR_FILE_READ_ERROR);

    FileNode mirror_radius_node = file["mirror_radius"];
    assertReturn(!mirror_radius_node.empty(), SPHERMIR_FILE_READ_ERROR);
    assertReturn(mirror_radius_node.isReal() || mirror_radius_node.isInt(), SPHERMIR_FILE_READ_ERROR);

    FileNode projector_position_x_node = file["projector_position_x"];
    assertReturn(!projector_position_x_node.empty(), SPHERMIR_FILE_READ_ERROR);
    assertReturn(projector_position_x_node.isReal() || projector_position_x_node.isInt(), SPHERMIR_FILE_READ_ERROR);

    FileNode projector_position_y_node = file["projector_position_y"];
    assertReturn(!projector_position_y_node.empty(), SPHERMIR_FILE_READ_ERROR);
    assertReturn(projector_position_y_node.isReal() || projector_position_y_node.isInt(), SPHERMIR_FILE_READ_ERROR);

    FileNode projector_position_z_node = file["projector_position_z"];
    assertReturn(!projector_position_z_node.empty(), SPHERMIR_FILE_READ_ERROR);
    assertReturn(projector_position_z_node.isReal() || projector_position_z_node.isInt(), SPHERMIR_FILE_READ_ERROR);

    FileNode projector_euler_x_node = file["projector_euler_x"];
    assertReturn(!projector_euler_x_node.empty(), SPHERMIR_FILE_READ_ERROR);
    assertReturn(projector_euler_x_node.isReal() || projector_euler_x_node.isInt(), SPHERMIR_FILE_READ_ERROR);

    FileNode projector_euler_y_node = file["projector_euler_y"];
    assertReturn(!projector_euler_y_node.empty(), SPHERMIR_FILE_READ_ERROR);
    assertReturn(projector_euler_y_node.isReal() || projector_euler_y_node.isInt(), SPHERMIR_FILE_READ_ERROR);

    FileNode projector_euler_z_node = file["projector_euler_z"];
    assertReturn(!projector_euler_z_node.empty(), SPHERMIR_FILE_READ_ERROR);
    assertReturn(projector_euler_z_node.isReal() || projector_euler_z_node.isInt(), SPHERMIR_FILE_READ_ERROR);

    FileNode projector_throw_ratio_node = file["projector_throw_ratio"];
    assertReturn(!projector_throw_ratio_node.empty(), SPHERMIR_FILE_READ_ERROR);
    assertReturn(projector_throw_ratio_node.isReal() || projector_throw_ratio_node.isInt(), SPHERMIR_FILE_READ_ERROR);

    FileNode source_scale_node = file["source_scale"];
    assertReturn(!source_scale_node.empty(), SPHERMIR_FILE_READ_ERROR);
    assertReturn(source_scale_node.isReal() || source_scale_node.isInt(), SPHERMIR_FILE_READ_ERROR);

    FileNode source_angle_node = file["source_angle"];
    assertReturn(!source_angle_node.empty(), SPHERMIR_FILE_READ_ERROR);
    assertReturn(source_angle_node.isReal() || source_angle_node.isInt(), SPHERMIR_FILE_READ_ERROR);

    FileNode source_mirrored_node = file["source_mirrored"];
    assertReturn(!source_mirrored_node.empty(), SPHERMIR_FILE_READ_ERROR);
    assertReturn(source_mirrored_node.isInt(), SPHERMIR_FILE_READ_ERROR);

    read(output_width_node, params_.output_width, 1024);
    read(output_height_node, params_.output_height, 768);
    read(output_interpolation_node, params_.output_interpolation, SPHERMIR_INTER_LINEAR);

    read(dome_radius_node, params_.dome_radius, 2.5);
    read(dome_angle_node, params_.dome_angle, 0.0);

    read(mirror_position_x_node, params_.mirror_position[0], 2.5);
    read(mirror_position_y_node, params_.mirror_position[1], 0.0);
    read(mirror_position_z_node, params_.mirror_position[2], 0.0);
    read(mirror_radius_node, params_.mirror_radius, 0.5);

    read(projector_position_x_node, params_.projector_position[0], 2.3);
    read(projector_position_y_node, params_.projector_position[1], 0.0);
    read(projector_position_z_node, params_.projector_position[2], 0.0);
    read(projector_euler_x_node, params_.projector_euler[0], 0.0);
    read(projector_euler_y_node, params_.projector_euler[1], 0.0);
    read(projector_euler_z_node, params_.projector_euler[2], 0.0);
    read(projector_throw_ratio_node, params_.projector_throw_ratio, 1.6);

    read(source_scale_node, params_.source_scale, 1.0);
    read(source_angle_node, params_.source_angle, 0.0);
    read(source_mirrored_node, params_.source_mirrored, 0);

    calc_maps();

    return SPHERMIR_SUCCESS;
}

sphermir_error_t sphermir_engine_t::save_params(const char* file_name)
{
    FileStorage file(file_name, FileStorage::WRITE);
    assertReturn(file.isOpened(), SPHERMIR_FILE_OPEN_ERROR);

    write(file, "output_width", params_.output_width);
    write(file, "output_height", params_.output_height);
    write(file, "output_interpolation", params_.output_interpolation);

    write(file, "dome_radius", params_.dome_radius);
    write(file, "dome_angle", params_.dome_angle);

    write(file, "mirror_position_x", params_.mirror_position[0]);
    write(file, "mirror_position_y", params_.mirror_position[1]);
    write(file, "mirror_position_z", params_.mirror_position[2]);
    write(file, "mirror_radius", params_.mirror_radius);

    write(file, "projector_position_x", params_.projector_position[0]);
    write(file, "projector_position_y", params_.projector_position[1]);
    write(file, "projector_position_z", params_.projector_position[2]);
    write(file, "projector_euler_x", params_.projector_euler[0]);
    write(file, "projector_euler_y", params_.projector_euler[1]);
    write(file, "projector_euler_z", params_.projector_euler[2]);
    write(file, "projector_throw_ratio", params_.projector_throw_ratio);

    write(file, "source_scale", params_.source_scale);
    write(file, "source_angle", params_.source_angle);
    write(file, "source_mirrored", params_.source_mirrored);

    return SPHERMIR_SUCCESS;
}

sphermir_error_t sphermir_engine_t::set_params(const sphermir_parameters_t& params)
{
    assertReturn(params.output_width > 0, SPHERMIR_PARAMS_ERROR);
    assertReturn(params.output_height > 0, SPHERMIR_PARAMS_ERROR);
    assertReturn(params.output_interpolation >= 0 && params.output_interpolation < 3, SPHERMIR_PARAMS_ERROR);

    params_ = params;

    calc_maps();

    return SPHERMIR_SUCCESS;
}

void sphermir_engine_t::get_params(sphermir_parameters_t* params)
{
    *params = params_;
}

void sphermir_engine_t::convert(Mat src, Mat dst)
{
    multiply(mapx_, src.cols - 1, src_mapx_);
    multiply(mapy_, src.rows - 1, src_mapy_);

    if (dst.rows == params_.output_height && dst.cols == params_.output_width)
    {
        remap(src, dst, src_mapx_, src_mapy_, params_.output_interpolation);
    }
    else
    {
        remap(src, out_, src_mapx_, src_mapy_, params_.output_interpolation);
        resize(out_, dst, dst.size(), 0.0, 0.0, params_.output_interpolation);
    }
}
