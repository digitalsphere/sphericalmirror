#include "spherical_mirror.h"
#include "engine.h"

using namespace std;
using namespace cv;

#define assertReturn(expr, code) if (!(expr)) return (code)

sphermir_error_t sphermir_create(sphermir_engine_t** engine)
{
    assertReturn(engine, SPHERMIR_NULL_POINTER_ERROR);

    try
    {
        *engine = new sphermir_engine_t;
    }
    catch(const bad_alloc&)
    {
        return SPHERMIR_ALLOCATION_ERROR;
    }
    catch(...)
    {
        return SPHERMIR_UNKNOWN_ERROR;
    }

    return SPHERMIR_SUCCESS;
}

sphermir_error_t sphermir_release(sphermir_engine_t* engine)
{
    assertReturn(engine, SPHERMIR_NULL_POINTER_ERROR);

    delete engine;

    return SPHERMIR_SUCCESS;
}

sphermir_error_t sphermir_load_params(sphermir_engine_t* engine, const char* file_name)
{
    assertReturn(engine, SPHERMIR_NULL_POINTER_ERROR);

    return engine->load_params(file_name);
}

sphermir_error_t sphermir_save_params(sphermir_engine_t* engine, const char* file_name)
{
    assertReturn(engine, SPHERMIR_NULL_POINTER_ERROR);

    return engine->save_params(file_name);
}

sphermir_error_t sphermir_set_params(sphermir_engine_t* engine, const sphermir_parameters_t* params)
{
    assertReturn(engine, SPHERMIR_NULL_POINTER_ERROR);

    return engine->set_params(*params);
}

sphermir_error_t sphermir_get_params(sphermir_engine_t* engine, sphermir_parameters_t* params)
{
    assertReturn(engine, SPHERMIR_NULL_POINTER_ERROR);

    engine->get_params(params);

    return SPHERMIR_SUCCESS;
}

sphermir_error_t sphermir_convert_8uc1(sphermir_engine_t* engine,
                                       const unsigned char* src, int src_rows, int src_cols, size_t src_step,
                                       unsigned char* dst, int dst_rows, int dst_cols, size_t dst_step)
{
    assertReturn(engine, SPHERMIR_NULL_POINTER_ERROR);
    assertReturn(src && dst, SPHERMIR_NULL_POINTER_ERROR);
    assertReturn(src_rows > 0 && src_cols > 0 && dst_rows > 0 && dst_cols > 0, SPHERMIR_SIZE_ERROR);
    assertReturn(src_step >= src_cols * sizeof(unsigned char) && dst_step >= dst_cols * sizeof(unsigned char), SPHERMIR_STEP_ERROR);

    engine->convert(Mat(src_rows, src_cols, CV_8UC1, (void*)src, src_step),
                    Mat(dst_rows, dst_cols, CV_8UC1, (void*)dst, dst_step));

    return SPHERMIR_SUCCESS;
}

sphermir_error_t sphermir_convert_8uc3(sphermir_engine_t* engine,
                                       const unsigned char* src, int src_rows, int src_cols, size_t src_step,
                                       unsigned char* dst, int dst_rows, int dst_cols, size_t dst_step)
{
    assertReturn(engine, SPHERMIR_NULL_POINTER_ERROR);
    assertReturn(src && dst, SPHERMIR_NULL_POINTER_ERROR);
    assertReturn(src_rows > 0 && src_cols > 0 && dst_rows > 0 && dst_cols > 0, SPHERMIR_SIZE_ERROR);
    assertReturn(src_step >= src_cols * 3 * sizeof(unsigned char) && dst_step >= dst_cols * 3 * sizeof(unsigned char), SPHERMIR_STEP_ERROR);

    engine->convert(Mat(src_rows, src_cols, CV_8UC3, (void*)src, src_step),
                    Mat(dst_rows, dst_cols, CV_8UC3, (void*)dst, dst_step));

    return SPHERMIR_SUCCESS;
}

sphermir_error_t sphermir_convert_8uc4(sphermir_engine_t* engine,
                                       const unsigned char* src, int src_rows, int src_cols, size_t src_step,
                                       unsigned char* dst, int dst_rows, int dst_cols, size_t dst_step)
{
    assertReturn(engine, SPHERMIR_NULL_POINTER_ERROR);
    assertReturn(src && dst, SPHERMIR_NULL_POINTER_ERROR);
    assertReturn(src_rows > 0 && src_cols > 0 && dst_rows > 0 && dst_cols > 0, SPHERMIR_SIZE_ERROR);
    assertReturn(src_step >= src_cols * 4 * sizeof(unsigned char) && dst_step >= dst_cols * 4 * sizeof(unsigned char), SPHERMIR_STEP_ERROR);

    engine->convert(Mat(src_rows, src_cols, CV_8UC4, (void*)src, src_step),
                    Mat(dst_rows, dst_cols, CV_8UC4, (void*)dst, dst_step));

    return SPHERMIR_SUCCESS;
}

sphermir_error_t sphermir_convert_16uc1(sphermir_engine_t* engine,
                                        const unsigned short* src, int src_rows, int src_cols, size_t src_step,
                                        unsigned short* dst, int dst_rows, int dst_cols, size_t dst_step)
{
    assertReturn(engine, SPHERMIR_NULL_POINTER_ERROR);
    assertReturn(src && dst, SPHERMIR_NULL_POINTER_ERROR);
    assertReturn(src_rows > 0 && src_cols > 0 && dst_rows > 0 && dst_cols > 0, SPHERMIR_SIZE_ERROR);
    assertReturn(src_step >= src_cols * sizeof(unsigned short) && dst_step >= dst_cols * sizeof(unsigned short), SPHERMIR_STEP_ERROR);

    engine->convert(Mat(src_rows, src_cols, CV_16UC1, (void*)src, src_step),
                    Mat(dst_rows, dst_cols, CV_16UC1, (void*)dst, dst_step));

    return SPHERMIR_SUCCESS;
}

sphermir_error_t sphermir_convert_16uc3(sphermir_engine_t* engine,
                                        const unsigned short* src, int src_rows, int src_cols, size_t src_step,
                                        unsigned short* dst, int dst_rows, int dst_cols, size_t dst_step)
{
    assertReturn(engine, SPHERMIR_NULL_POINTER_ERROR);
    assertReturn(src && dst, SPHERMIR_NULL_POINTER_ERROR);
    assertReturn(src_rows > 0 && src_cols > 0 && dst_rows > 0 && dst_cols > 0, SPHERMIR_SIZE_ERROR);
    assertReturn(src_step >= src_cols * 3 * sizeof(unsigned short) && dst_step >= dst_cols * 3 * sizeof(unsigned short), SPHERMIR_STEP_ERROR);

    engine->convert(Mat(src_rows, src_cols, CV_16UC3, (void*)src, src_step),
                    Mat(dst_rows, dst_cols, CV_16UC3, (void*)dst, dst_step));

    return SPHERMIR_SUCCESS;
}

sphermir_error_t sphermir_convert_16uc4(sphermir_engine_t* engine,
                                        const unsigned short* src, int src_rows, int src_cols, size_t src_step,
                                        unsigned short* dst, int dst_rows, int dst_cols, size_t dst_step)
{
    assertReturn(engine, SPHERMIR_NULL_POINTER_ERROR);
    assertReturn(src && dst, SPHERMIR_NULL_POINTER_ERROR);
    assertReturn(src_rows > 0 && src_cols > 0 && dst_rows > 0 && dst_cols > 0, SPHERMIR_SIZE_ERROR);
    assertReturn(src_step >= src_cols * 4 * sizeof(unsigned short) && dst_step >= dst_cols * 4 * sizeof(unsigned short), SPHERMIR_STEP_ERROR);

    engine->convert(Mat(src_rows, src_cols, CV_16UC4, (void*)src, src_step),
                    Mat(dst_rows, dst_cols, CV_16UC4, (void*)dst, dst_step));

    return SPHERMIR_SUCCESS;
}

sphermir_error_t sphermir_convert_32fc1(sphermir_engine_t* engine,
                                        const float* src, int src_rows, int src_cols, size_t src_step,
                                        float* dst, int dst_rows, int dst_cols, size_t dst_step)
{
    assertReturn(engine, SPHERMIR_NULL_POINTER_ERROR);
    assertReturn(src && dst, SPHERMIR_NULL_POINTER_ERROR);
    assertReturn(src_rows > 0 && src_cols > 0 && dst_rows > 0 && dst_cols > 0, SPHERMIR_SIZE_ERROR);
    assertReturn(src_step >= src_cols * sizeof(float) && dst_step >= dst_cols * sizeof(float), SPHERMIR_STEP_ERROR);

    engine->convert(Mat(src_rows, src_cols, CV_32FC1, (void*)src, src_step),
                    Mat(dst_rows, dst_cols, CV_32FC1, (void*)dst, dst_step));

    return SPHERMIR_SUCCESS;
}

sphermir_error_t sphermir_convert_32fc3(sphermir_engine_t* engine,
                                        const float* src, int src_rows, int src_cols, size_t src_step,
                                        float* dst, int dst_rows, int dst_cols, size_t dst_step)
{
    assertReturn(engine, SPHERMIR_NULL_POINTER_ERROR);
    assertReturn(src && dst, SPHERMIR_NULL_POINTER_ERROR);
    assertReturn(src_rows > 0 && src_cols > 0 && dst_rows > 0 && dst_cols > 0, SPHERMIR_SIZE_ERROR);
    assertReturn(src_step >= src_cols * 3 * sizeof(float) && dst_step >= dst_cols * 3 * sizeof(float), SPHERMIR_STEP_ERROR);

    engine->convert(Mat(src_rows, src_cols, CV_32FC3, (void*)src, src_step),
                    Mat(dst_rows, dst_cols, CV_32FC3, (void*)dst, dst_step));

    return SPHERMIR_SUCCESS;
}

sphermir_error_t sphermir_convert_32fc4(sphermir_engine_t* engine,
                                        const float* src, int src_rows, int src_cols, size_t src_step,
                                        float* dst, int dst_rows, int dst_cols, size_t dst_step)
{
    assertReturn(engine, SPHERMIR_NULL_POINTER_ERROR);
    assertReturn(src && dst, SPHERMIR_NULL_POINTER_ERROR);
    assertReturn(src_rows > 0 && src_cols > 0 && dst_rows > 0 && dst_cols > 0, SPHERMIR_SIZE_ERROR);
    assertReturn(src_step >= src_cols * 4 * sizeof(float) && dst_step >= dst_cols * 4 * sizeof(float), SPHERMIR_STEP_ERROR);

    engine->convert(Mat(src_rows, src_cols, CV_32FC4, (void*)src, src_step),
                    Mat(dst_rows, dst_cols, CV_32FC4, (void*)dst, dst_step));

    return SPHERMIR_SUCCESS;
}
