#define _USE_MATH_DEFINES
#include <cmath>
#include "engine.h"

using namespace std;
using namespace cv;

namespace
{
    inline double deg2rad(double x)
    {
        return x * 0.01745329251994329576923690768489;
    }

    inline Point3d normalize(const Point3d& p)
    {
        double len = sqrt(p.dot(p));
        return Point3d(p.x / len, p.y / len, p.z / len);
    }

    inline Point3d reflect(const Point3d& incident, const Point3d& normal)
    {
        return incident - normal * 2.0 * incident.dot(normal);
    }

    inline Point3d z_axis_rotation(double a, const Point3d& v)
    {
        double cosa = cos(a), sina = sin(a);
        return Point3d
            (
                cosa * v.x - sina * v.y,
                sina * v.x + cosa * v.y,
                v.z
            );
    }

    inline Point3d y_axis_rotation(double a, const Point3d& v)
    {
        double cosa = cos(a), sina = sin(a);
        return Point3d
            (
                cosa * v.x + sina * v.z,
                v.y,
               -sina * v.x + cosa * v.z
            );
    }

    inline Point3d x_axis_rotation(double a, const Point3d& v)
    {
        double cosa = cos(a), sina = sin(a);
        return Point3d
            (
                v.x,
                cosa * v.y - sina * v.z,
                sina * v.y + cosa * v.z
            );
    }

    inline Point3d RzRyRxV(const Point3d& a, const Point3d& v)
    {
        return
            z_axis_rotation(a.z,
                y_axis_rotation(a.y,
                    x_axis_rotation(a.x, v)));
    }

    struct Ray
    {
        Point3d Origin;
        Point3d Direction;
    };

    void getCameraVectors(const sphermir_parameters_t& params, Point3d vectors[3])
    {
        Point3d Mp(Vec3d(params.mirror_position));
        Point3d Pp(Vec3d(params.projector_position));
        Point3d euler(deg2rad(params.projector_euler[0]), deg2rad(params.projector_euler[1]), deg2rad(params.projector_euler[2]));

        Point3d view = Mp - Pp;
        view.z = 0.0;
        view = normalize(view);

        Point3d up(0.0, 0.0, 1.0);

        Point3d side = up.cross(view);

        vectors[0] = RzRyRxV(euler, view);
        vectors[1] = RzRyRxV(euler, side);
        vectors[2] = RzRyRxV(euler, up);
    }

    inline Point2d calcScale(double fov, double aspect)
    {
        double tangent = tan(fov / 2.0);

        return Point2d(tangent, aspect * tangent);
    }

    inline void GenerateRay(Ray& ray,
                 const Point3d& view,
                 const Point3d& up,
                 const Point3d& side,
                 const Point3d& position,
                 const Point2d& scale,
                 int width,
                 int height,
                 int i, int j)
    {
        Point2d screen
            (
                (static_cast<double>(i) / height      ) * scale.x,
                (static_cast<double>(j) / width  - 0.5) * scale.y
            );

        ray.Direction = normalize(view + side * screen.x + up * screen.y);

        ray.Origin = position;
    }

    bool IntersectSphere(const Ray& ray        /* ray origin and direction */,
                         double radius         /* sphere radius */,
                         const Point3d& center /* sphere center */,
                         double& start         /* time of 1st intersection */,
                         double& final         /* time of 2nd intersection */ )
    {
        Point3d origin = ray.Origin - center;

        double A = ray.Direction.dot(ray.Direction);

        double B = ray.Direction.dot(origin);

        double C = origin.dot(origin) - radius * radius;

        double D = B * B - A * C;

        if (D > 0.0)
        {
            D = sqrt(D);

            start = max(0.0, -(B + D) / A );

            final = (D - B) / A;

            return final > 0.0;
        }

        return false;
    }

    Point2d Raytrace(const Ray& ray, const sphermir_parameters_t& params)
    {
        static const Point2d out(-1.0, -1.0);

        double t1, t2;

        Point3d Mp(Vec3d(params.mirror_position));
        Point3d Pp(Vec3d(params.projector_position));

        if (!IntersectSphere(ray, params.mirror_radius, Mp, t1, t2))
            return out;

        Point3d q = Pp + ray.Direction * t1;

        Point3d N = normalize(q - Mp);

        Point3d d2 = reflect(ray.Direction, N);

        double t3, t4;

        Ray ray2; ray2.Origin = q; ray2.Direction = d2;
        if (!IntersectSphere(ray2, params.dome_radius, Vec3d(0.0, 0.0, 0.0), t3, t4))
            return out;

        Point3d Q1 = q + d2 * t4;

        Point3d Q2 = y_axis_rotation(deg2rad(params.dome_angle), Q1);
        Point3d Q3( Q2.x / params.dome_radius,
                    Q2.y / params.dome_radius,
                    Q2.z / params.dome_radius );

        double r = params.source_scale * (acos(Q3.z) / M_PI_2);

        double pa = atan2(Q3.y, Q3.x) + deg2rad(params.source_angle);

        return Point2d((r * cos(pa)) / 2.0 + 0.5, (r * sin(pa)) / 2.0 + 0.5);
    }

    struct CalcMapsBody : ParallelLoopBody
    {
        void operator() (const Range& range) const;

        mutable Mat mapx_;
        mutable Mat mapy_;

        const sphermir_parameters_t* params_;
    };

    void CalcMapsBody::operator() (const Range& range) const
    {
        double aspect = static_cast<double>(params_->output_width) / params_->output_height;

        Point3d vectors[3];
        getCameraVectors(*params_, vectors);

        Point3d Pp(Vec3d(params_->projector_position));

        Point2d scale = calcScale(2.0 * atan(0.5 / params_->projector_throw_ratio) / aspect, aspect) * 2.0;

        Ray ray;

        for (int i = range.start; i < range.end; ++i)
        {
            float* mapx_row = mapx_.ptr<float>(i);
            float* mapy_row = mapy_.ptr<float>(i);

            for (int j = 0; j < mapx_.cols; ++j)
            {
                GenerateRay(ray, vectors[0], vectors[1], vectors[2], Pp, scale,
                    params_->output_width, params_->output_height,
                    params_->output_height - i - 1, params_->source_mirrored ? params_->output_width - j - 1 : j);

                Point2d out = Raytrace(ray, *params_);

                mapx_row[j] = static_cast<float>(out.x);
                mapy_row[j] = static_cast<float>(out.y);
            }
        }
    }
}

void sphermir_engine_t::calc_maps()
{
    mapx_.create(params_.output_height, params_.output_width, CV_32FC1);
    mapy_.create(params_.output_height, params_.output_width, CV_32FC1);

    CalcMapsBody body;
    body.mapx_ = mapx_;
    body.mapy_ = mapy_;
    body.params_ = &params_;

    parallel_for_(Range(0, mapx_.rows), body);
}
