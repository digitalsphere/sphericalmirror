#ifndef __ENGINE_H__
#define __ENGINE_H__

#include <vector>
#include <opencv2/core/core.hpp>
#include "spherical_mirror.h"

struct sphermir_engine_t
{
public:
    sphermir_engine_t();

    sphermir_error_t load_params(const char* file_name);
    sphermir_error_t save_params(const char* file_name);

    sphermir_error_t set_params(const sphermir_parameters_t& params);
    void get_params(sphermir_parameters_t* params);

    void convert(cv::Mat src, cv::Mat dst);

private:
    void calc_maps();

    sphermir_parameters_t params_;

    cv::Mat out_;
    cv::Mat mapx_;
    cv::Mat mapy_;

    cv::Mat src_mapx_;
    cv::Mat src_mapy_;
};

#endif // __ENGINE_H__
