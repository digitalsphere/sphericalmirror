#include <stdexcept>
#include <sstream>

#include <cuda_runtime_api.h>

#include <opencv2/gpu/devmem2d.hpp>

#include "spherical_mirror.h"

namespace kernel
{
    enum {
        VIEW_X = 0,
        VIEW_Y,
        VIEW_Z,

        UP_X,
        UP_Y,
        UP_Z,

        SIDE_X,
        SIDE_Y,
        SIDE_Z,

        SCALE_X,
        SCALE_Y,

        OUTPUT_WIDTH,
        OUTPUT_HEIGHT,

        DOME_RADIUS,
        DOME_ANGLE,

        MP_X,
        MP_Y,
        MP_Z,
        MIRROR_RADIUS,

        PP_X,
        PP_Y,
        PP_Z,

        SOURCE_MIRRORED,
        SOURCE_SCALE,
        SOURCE_ANGLE,

        PARAMS_LEN
    };

    __constant__ float c_params[PARAMS_LEN];

    __host__ float3 z_axis_rotation(float a, const float3& v)
    {
        float cosa = cosf(a), sina = sinf(a);
        return make_float3
            (
                cosa * v.x - sina * v.y,
                sina * v.x + cosa * v.y,
                v.z
            );
    }

    __host__ __device__ __forceinline__ float3 y_axis_rotation(float a, const float3& v)
    {
        float cosa = cosf(a), sina = sinf(a);
        return make_float3
            (
                cosa * v.x + sina * v.z,
                v.y,
               -sina * v.x + cosa * v.z
            );
    }

    __host__ float3 x_axis_rotation(float a, const float3& v)
    {
        float cosa = cosf(a), sina = sinf(a);
        return make_float3
            (
                v.x,
                cosa * v.y - sina * v.z,
                sina * v.y + cosa * v.z
            );
    }

    __host__ float3 RzRyRxV(const float3& a, const float3& v)
    {
        return
            z_axis_rotation(a.z,
                y_axis_rotation(a.y,
                    x_axis_rotation(a.x, v)));
    }

    __device__ __forceinline__ float deg2rad(float x)
    {
        return x * 0.01745329251994329576923690768489f;
    }

    __host__ __device__ __forceinline__ float dot(const float3& a, const float3& b)
    {
        return a.x * b.x + a.y * b.y + a.z * b.z;
    }

    __host__ __device__ __forceinline__ float3 normalize(const float3& p)
    {
        const float len = sqrtf(dot(p, p));
        return make_float3(p.x / len, p.y / len, p.z / len);
    }

    __device__ __forceinline__ float3 reflect(const float3& incident, const float3& normal)
    {
        const float d = 2.0f * dot(incident, normal);
        return make_float3(incident.x - d * normal.x, incident.y - d * normal.y, incident.z - d * normal.z);
    }

    struct Ray
    {
        float3 Origin;
        float3 Direction;
    };

    __device__ bool IntersectSphere(const Ray& ray       /* ray origin and direction */,
                                    float radius         /* sphere radius */,
                                    const float3& center /* sphere center */,
                                    float& start         /* time of 1st intersection */,
                                    float& final         /* time of 2nd intersection */ )
    {
        float3 origin = make_float3(ray.Origin.x - center.x, ray.Origin.y - center.y, ray.Origin.z - center.z);

        float A = dot(ray.Direction, ray.Direction);

        float B = dot(ray.Direction, origin);

        float C = dot(origin, origin) - radius * radius;

        float D = B * B - A * C;

        if (D > 0.0f)
        {
            D = sqrtf(D);

            start = fmax(0.0f, -(B + D) / A );

            final = (D - B) / A;

            return final > 0.0f;
        }

        return false;
    }

#define PI_2 1.57079632679489661923f

    __device__ float2 Raytrace(const Ray& ray)
    {
        float t1, t2;

        if (!IntersectSphere(ray, c_params[MIRROR_RADIUS], make_float3(c_params[MP_X], c_params[MP_Y], c_params[MP_Z]), t1, t2))
            return make_float2(-1.0f, -1.0f);

        float3 q = make_float3(c_params[PP_X] + ray.Direction.x * t1, c_params[PP_Y] + ray.Direction.y * t1, c_params[PP_Z] + ray.Direction.z * t1);

        float3 N = normalize(make_float3(q.x - c_params[MP_X], q.y - c_params[MP_Y], q.z - c_params[MP_Z]));

        float3 d2 = reflect(ray.Direction, N);

        float t3, t4;

        Ray ray2; ray2.Origin = q; ray2.Direction = d2;
        if (!IntersectSphere(ray2, c_params[DOME_RADIUS], make_float3(0.0, 0.0, 0.0), t3, t4))
            return make_float2(-1.0f, -1.0f);

        float3 Q1 = make_float3(q.x + d2.x * t4, q.y + d2.y * t4, q.z + d2.z * t4);

        float3 Q2 = y_axis_rotation(deg2rad(c_params[DOME_ANGLE]), Q1);
        float3 Q3 = make_float3(Q2.x / c_params[DOME_RADIUS], Q2.y / c_params[DOME_RADIUS], Q2.z / c_params[DOME_RADIUS] );

        float r = c_params[SOURCE_SCALE] * (acosf(Q3.z) / PI_2);

        float pa = atan2f(Q3.y, Q3.x) + deg2rad(c_params[SOURCE_ANGLE]);

        return make_float2((r * cosf(pa)) / 2.0f + 0.5f, (r * sinf(pa)) / 2.0f + 0.5f);
    }

    __device__ void GenerateRay(Ray& ray, int i, int j)
    {
        float2 screen = make_float2
            (
                (static_cast<float>(i) / c_params[OUTPUT_HEIGHT]      ) * c_params[SCALE_X],
                (static_cast<float>(j) / c_params[OUTPUT_WIDTH]  - 0.5) * c_params[SCALE_Y]
            );

        ray.Direction = make_float3
            (
                c_params[VIEW_X] + c_params[SIDE_X] * screen.x + c_params[UP_X] * screen.y,
                c_params[VIEW_Y] + c_params[SIDE_Y] * screen.x + c_params[UP_Y] * screen.y,
                c_params[VIEW_Z] + c_params[SIDE_Z] * screen.x + c_params[UP_Z] * screen.y
            );
        ray.Direction = normalize(ray.Direction);

        ray.Origin = make_float3(c_params[PP_X], c_params[PP_Y], c_params[PP_Z]);
    }

    __host__ float3 cross(const float3& a, const float3& b)
    {
        return make_float3(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x);
    }

    __host__ void getCameraVectors(const sphermir_parameters_t& params, float3 vectors[3])
    {
        float3 view = make_float3
            (
                static_cast<float>(params.mirror_position[0] - params.projector_position[0]),
                static_cast<float>(params.mirror_position[1] - params.projector_position[1]),
                static_cast<float>(params.mirror_position[2] - params.projector_position[2])
            );
        view.z = 0.0;
        view = normalize(view);

        float3 up = make_float3(0.0, 0.0, 1.0);

        float3 side = cross(up, view);

        float3 euler = make_float3(static_cast<float>(params.projector_euler[0]), static_cast<float>(params.projector_euler[1]), static_cast<float>(params.projector_euler[2]));

        vectors[0] = RzRyRxV(euler, view);
        vectors[1] = RzRyRxV(euler, side);
        vectors[2] = RzRyRxV(euler, up);
    }

    __host__ float2 calcScale(float fov, float aspect)
    {
        float tangent = tanf(fov / 2.0f);

        return make_float2(tangent * 2.0f, aspect * tangent * 2.0f);
    }

    __host__ void ___cudaSafeCall(cudaError_t err, const char *file, const int line)
    {
        if (cudaSuccess != err)
        {
            std::ostringstream ostr;
            ostr << "CUDA error: " << cudaGetErrorString(err) << " in file " << file << " line " << line;
            throw std::runtime_error(ostr.str().c_str());
        }
    }
}

using namespace kernel;

__global__ void calc_maps(cv::gpu::DevMem2Df mapx, cv::gpu::PtrStepf mapy)
{
    const int x = blockIdx.x * blockDim.x + threadIdx.x;
    const int y = blockIdx.y * blockDim.y + threadIdx.y;

    if (y < mapx.rows && x < mapx.cols)
    {
        Ray ray;
        GenerateRay(ray, c_params[OUTPUT_HEIGHT] - y - 1, c_params[SOURCE_MIRRORED] ? c_params[OUTPUT_WIDTH] - x - 1 : x);

        float2 out = Raytrace(ray);

        if (out.x >= 0)
        {
            out.x *= c_params[OUTPUT_WIDTH] - 1;
            out.y *= c_params[OUTPUT_HEIGHT] - 1;
        }

        mapx.ptr(y)[x] = out.x;
        mapy.ptr(y)[x] = out.y;
    }
}

#define cudaSafeCall(expr)  ___cudaSafeCall(expr, __FILE__, __LINE__)

__host__ void calc_maps_gpu(cv::gpu::DevMem2Df mapx, cv::gpu::PtrStepf mapy, const sphermir_parameters_t& params)
{
    float gpu_params[PARAMS_LEN];

    float3 vectors[3];
    getCameraVectors(params, vectors);

    gpu_params[VIEW_X] = vectors[0].x;
    gpu_params[VIEW_Y] = vectors[0].y;
    gpu_params[VIEW_Z] = vectors[0].z;

    gpu_params[UP_X] = vectors[1].x;
    gpu_params[UP_Y] = vectors[1].y;
    gpu_params[UP_Z] = vectors[1].z;

    gpu_params[SIDE_X] = vectors[2].x;
    gpu_params[SIDE_Y] = vectors[2].y;
    gpu_params[SIDE_Z] = vectors[2].z;

    float aspect = static_cast<float>(params.output_width) / params.output_height;
    float2 scale = calcScale(2.0f * atanf(0.5f / static_cast<float>(params.projector_throw_ratio)) / aspect, aspect);

    gpu_params[SCALE_X] = scale.x;
    gpu_params[SCALE_Y] = scale.y;

    gpu_params[OUTPUT_WIDTH] = static_cast<float>(params.output_width);
    gpu_params[OUTPUT_HEIGHT] = static_cast<float>(params.output_height);

    gpu_params[DOME_RADIUS] = static_cast<float>(params.dome_radius);
    gpu_params[DOME_ANGLE] = static_cast<float>(params.dome_angle);

    gpu_params[MP_X] = static_cast<float>(params.mirror_position[0]);
    gpu_params[MP_Y] = static_cast<float>(params.mirror_position[1]);
    gpu_params[MP_Z] = static_cast<float>(params.mirror_position[2]);
    gpu_params[MIRROR_RADIUS] = static_cast<float>(params.mirror_radius);

    gpu_params[PP_X] = static_cast<float>(params.projector_position[0]);
    gpu_params[PP_Y] = static_cast<float>(params.projector_position[1]);
    gpu_params[PP_Z] = static_cast<float>(params.projector_position[2]);

    gpu_params[SOURCE_MIRRORED] = static_cast<float>(params.source_mirrored);
    gpu_params[SOURCE_SCALE] = static_cast<float>(params.source_scale);
    gpu_params[SOURCE_ANGLE] = static_cast<float>(params.source_angle);

    cudaSafeCall( cudaMemcpyToSymbol(c_params, gpu_params, PARAMS_LEN * sizeof(float)) );

    const dim3 block(32, 8);
    const dim3 grid((mapx.cols + block.x - 1) / block.x, (mapx.rows + block.y - 1) / block.y);

    calc_maps<<< grid, block >>>(mapx, mapy);
    cudaSafeCall( cudaGetLastError() );

    cudaSafeCall( cudaDeviceSynchronize() );
}
