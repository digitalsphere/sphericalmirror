#ifndef __SPHERICAL_MIRROR_H__
#define __SPHERICAL_MIRROR_H__

#include <stddef.h>
#include "spherical_mirror_export.h"

////////////////////////////////////////////////////////////////
// define SPHERICAL_MIRROR_API

#if defined __cplusplus
#   define SPHERICAL_MIRROR_EXTERN extern "C"
#else
#   define SPHERICAL_MIRROR_EXTERN extern
#endif

#define SPHERICAL_MIRROR_API SPHERICAL_MIRROR_EXTERN SPHERICAL_MIRROR_EXPORT

////////////////////////////////////////////////////////////////
// API

// Spherical Mirror Calibration Engine
typedef struct sphermir_engine_t sphermir_engine_t;

// Interpolation types
enum
{
    SPHERMIR_INTER_NEAREST = 0, // nearest neighbor
    SPHERMIR_INTER_LINEAR  = 1, // bilinear
    SPHERMIR_INTER_CUBIC   = 2, // bicubic
};

// Calibration Parameters
typedef struct sphermir_parameters_t
{
    // Output parameters
    int output_width;
    int output_height;
    int output_interpolation;

    // Dome parameters
    double dome_radius;
    double dome_angle;

    // Mirror parameters
    double mirror_position[3];
    double mirror_radius;

    // Projector parameters
    double projector_position[3];
    double projector_euler[3];
    double projector_throw_ratio;

    // Source parameters
    double source_scale;
    double source_angle;
    int source_mirrored;
} sphermir_parameters_t;

// Error codes
typedef enum sphermir_error_t
{
    SPHERMIR_SUCCESS            =  0, // Success
    SPHERMIR_FILE_OPEN_ERROR    = -1, // Can't open file
    SPHERMIR_FILE_READ_ERROR    = -2, // Incorrect file format
    SPHERMIR_PARAMS_ERROR       = -3, // Incorrect parameters
    SPHERMIR_NULL_POINTER_ERROR = -4, // Passed null pointer
    SPHERMIR_ALLOCATION_ERROR   = -5, // Can't allocate memory
    SPHERMIR_SIZE_ERROR         = -6, // Incorrect size
    SPHERMIR_STEP_ERROR         = -7, // Incorrect step
    SPHERMIR_UNKNOWN_ERROR      = -8  // Unknown error
} sphermir_error_t;

// Create engine with default parameters
// Returns error code: SPHERMIR_SUCCESS, SPHERMIR_NULL_POINTER_ERROR, SPHERMIR_ALLOCATION_ERROR, SPHERMIR_UNKNOWN_ERROR
SPHERICAL_MIRROR_API sphermir_error_t sphermir_create(sphermir_engine_t** engine);

// Release engine and free memory
// Returns error code: SPHERMIR_SUCCESS, SPHERMIR_NULL_POINTER_ERROR
SPHERICAL_MIRROR_API sphermir_error_t sphermir_release(sphermir_engine_t* engine);

// Load parameters from file
// Returns error code: SPHERMIR_SUCCESS, SPHERMIR_NULL_POINTER_ERROR, SPHERMIR_FILE_OPEN_ERROR, SPHERMIR_FILE_READ_ERROR
SPHERICAL_MIRROR_API sphermir_error_t sphermir_load_params(sphermir_engine_t* engine, const char* file_name);

// Save parameters to file
// Returns error code: SPHERMIR_SUCCESS, SPHERMIR_NULL_POINTER_ERROR, SPHERMIR_FILE_OPEN_ERROR
SPHERICAL_MIRROR_API sphermir_error_t sphermir_save_params(sphermir_engine_t* engine, const char* file_name);

// Set parameters
// Returns error code: SPHERMIR_SUCCESS, SPHERMIR_NULL_POINTER_ERROR, SPHERMIR_PARAMS_ERROR
SPHERICAL_MIRROR_API sphermir_error_t sphermir_set_params(sphermir_engine_t* engine, const sphermir_parameters_t* params);

// Get parameters
// Returns error code: SPHERMIR_SUCCESS, SPHERMIR_NULL_POINTER_ERROR
SPHERICAL_MIRROR_API sphermir_error_t sphermir_get_params(sphermir_engine_t* engine, sphermir_parameters_t* params);

// Convert 8uc1 source image
// Returns error code: SPHERMIR_SUCCESS, SPHERMIR_NULL_POINTER_ERROR, SPHERMIR_SIZE_ERROR, SPHERMIR_STEP_ERROR
SPHERICAL_MIRROR_API sphermir_error_t sphermir_convert_8uc1(sphermir_engine_t* engine,
                                                    const unsigned char* src, int src_rows, int src_cols, size_t src_step,
                                                    unsigned char* dst, int dst_rows, int dst_cols, size_t dst_step);

// Convert 8uc3 source image
// Returns error code: SPHERMIR_SUCCESS, SPHERMIR_NULL_POINTER_ERROR, SPHERMIR_SIZE_ERROR, SPHERMIR_STEP_ERROR
SPHERICAL_MIRROR_API sphermir_error_t sphermir_convert_8uc3(sphermir_engine_t* engine,
                                                    const unsigned char* src, int src_rows, int src_cols, size_t src_step,
                                                    unsigned char* dst, int dst_rows, int dst_cols, size_t dst_step);

// Convert 8uc4 source image
// Returns error code: SPHERMIR_SUCCESS, SPHERMIR_NULL_POINTER_ERROR, SPHERMIR_SIZE_ERROR, SPHERMIR_STEP_ERROR
SPHERICAL_MIRROR_API sphermir_error_t sphermir_convert_8uc4(sphermir_engine_t* engine,
                                                    const unsigned char* src, int src_rows, int src_cols, size_t src_step,
                                                    unsigned char* dst, int dst_rows, int dst_cols, size_t dst_step);

// Convert 16uc1 source image
// Returns error code: SPHERMIR_SUCCESS, SPHERMIR_NULL_POINTER_ERROR, SPHERMIR_SIZE_ERROR, SPHERMIR_STEP_ERROR
SPHERICAL_MIRROR_API sphermir_error_t sphermir_convert_16uc1(sphermir_engine_t* engine,
                                                     const unsigned short* src, int src_rows, int src_cols, size_t src_step,
                                                     unsigned short* dst, int dst_rows, int dst_cols, size_t dst_step);

// Convert 16uc3 source image
// Returns error code: SPHERMIR_SUCCESS, SPHERMIR_NULL_POINTER_ERROR, SPHERMIR_SIZE_ERROR, SPHERMIR_STEP_ERROR
SPHERICAL_MIRROR_API sphermir_error_t sphermir_convert_16uc3(sphermir_engine_t* engine,
                                                     const unsigned short* src, int src_rows, int src_cols, size_t src_step,
                                                     unsigned short* dst, int dst_rows, int dst_cols, size_t dst_step);

// Convert 16uc4 source image
// Returns error code: SPHERMIR_SUCCESS, SPHERMIR_NULL_POINTER_ERROR, SPHERMIR_SIZE_ERROR, SPHERMIR_STEP_ERROR
SPHERICAL_MIRROR_API sphermir_error_t sphermir_convert_16uc4(sphermir_engine_t* engine,
                                                     const unsigned short* src, int src_rows, int src_cols, size_t src_step,
                                                     unsigned short* dst, int dst_rows, int dst_cols, size_t dst_step);

// Convert 32fc1 source image
// Returns error code: SPHERMIR_SUCCESS, SPHERMIR_NULL_POINTER_ERROR, SPHERMIR_SIZE_ERROR, SPHERMIR_STEP_ERROR
SPHERICAL_MIRROR_API sphermir_error_t sphermir_convert_32fc1(sphermir_engine_t* engine,
                                                     const float* src, int src_rows, int src_cols, size_t src_step,
                                                     float* dst, int dst_rows, int dst_cols, size_t dst_step);

// Convert 32fc3 source image
// Returns error code: SPHERMIR_SUCCESS, SPHERMIR_NULL_POINTER_ERROR, SPHERMIR_SIZE_ERROR, SPHERMIR_STEP_ERROR
SPHERICAL_MIRROR_API sphermir_error_t sphermir_convert_32fc3(sphermir_engine_t* engine,
                                                     const float* src, int src_rows, int src_cols, size_t src_step,
                                                     float* dst, int dst_rows, int dst_cols, size_t dst_step);

// Convert 32fc4 source image
// Returns error code: SPHERMIR_SUCCESS, SPHERMIR_NULL_POINTER_ERROR, SPHERMIR_SIZE_ERROR, SPHERMIR_STEP_ERROR
SPHERICAL_MIRROR_API sphermir_error_t sphermir_convert_32fc4(sphermir_engine_t* engine,
                                                     const float* src, int src_rows, int src_cols, size_t src_step,
                                                     float* dst, int dst_rows, int dst_cols, size_t dst_step);

#endif // __SPHERICAL_MIRROR_H__
