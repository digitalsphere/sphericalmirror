#include <stdexcept>

#include <QtCore>
#include <QtGui>

#include "ConfigWidget.h"
#include "ui_ConfigWidget.h"

ConfigWidget::ConfigWidget(QWidget* parent) : QWidget(parent), m_ui(new Ui::ConfigWidget)
{
    m_ui->setupUi(this);

    m_imgDialog = new QDialog(this);
    m_imgLbl = new QLabel;
    m_imgLbl->setScaledContents(true);
    QHBoxLayout* layout = new QHBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(m_imgLbl);
    m_imgDialog->setLayout(layout);

    loadConfig();

    if (sphermir_create(&engine) != SPHERMIR_SUCCESS)
        throw std::runtime_error("Can't create engine");

    loadParams();
    loadImage(imgFile);
}

ConfigWidget::~ConfigWidget()
{
    delete m_ui;
    sphermir_release(engine);
}

void ConfigWidget::loadParams()
{
    if (!paramsFile.isEmpty())
    {
        sphermir_error_t err = sphermir_load_params(engine, paramsFile.toAscii().data());

        if (err == SPHERMIR_FILE_OPEN_ERROR)
        {
            QMessageBox::critical(this, tr("Error"), tr("Can't open parameters file %1").arg(paramsFile));
            paramsFile.clear();
            return;
        }
        if (err == SPHERMIR_FILE_READ_ERROR)
        {
            QMessageBox::critical(this, tr("Error"), tr("Incorrect parameters file %1").arg(paramsFile));
            paramsFile.clear();
            return;
        }

        sphermir_parameters_t params;
        sphermir_get_params(engine, &params);

        setParametersOnUI(params);

        QFileInfo fileInfo(paramsFile);
        paramsDir = fileInfo.dir().absolutePath();
    }
}

void ConfigWidget::loadImage(const QString& fileName)
{
    if (!fileName.isEmpty())
    {
        if (m_image.load(fileName))
        {
            m_image = m_image.convertToFormat(QImage::Format_ARGB32);

            QFileInfo fileInfo(fileName);
            setWindowTitle(fileInfo.fileName());
            imgDir = fileInfo.dir().absolutePath();

            m_ui->convertBtn->setEnabled(true);
        }
        else
        {
            setWindowTitle(tr("No image"));

            m_ui->convertBtn->setEnabled(false);
        }
    }
}

sphermir_parameters_t ConfigWidget::getParametersFromUI()
{
    sphermir_parameters_t params;

    params.output_width = m_ui->output_width->value();
    params.output_height = m_ui->output_height->value();
    params.output_interpolation = m_ui->output_interpolation->currentIndex();

    params.dome_radius = m_ui->dome_radius->value();
    params.dome_angle = m_ui->dome_angle->value();

    params.mirror_position[0] = m_ui->mirror_position_x->value();
    params.mirror_position[1] = m_ui->mirror_position_y->value();
    params.mirror_position[2] = m_ui->mirror_position_z->value();
    params.mirror_radius = m_ui->mirror_radius->value();

    params.projector_position[0] = m_ui->projector_position_x->value();
    params.projector_position[1] = m_ui->projector_position_y->value();
    params.projector_position[2] = m_ui->projector_position_z->value();
    params.projector_euler[0] = m_ui->projector_euler_x->value();
    params.projector_euler[1] = m_ui->projector_euler_y->value();
    params.projector_euler[2] = m_ui->projector_euler_z->value();
    params.projector_throw_ratio = m_ui->projector_throw_ratio->value();

    params.source_scale = m_ui->source_scale->value();
    params.source_angle = m_ui->source_angle->value();
    params.source_mirrored = m_ui->source_mirrored->isChecked();

    return params;
}

void ConfigWidget::setParametersOnUI(const sphermir_parameters_t &params)
{
    m_ui->output_width->setValue(params.output_width);
    m_ui->output_height->setValue(params.output_height);
    m_ui->output_interpolation->setCurrentIndex(params.output_interpolation);

    m_ui->dome_radius->setValue(params.dome_radius);
    m_ui->dome_angle->setValue(params.dome_angle);

    m_ui->mirror_position_x->setValue(params.mirror_position[0]);
    m_ui->mirror_position_y->setValue(params.mirror_position[1]);
    m_ui->mirror_position_z->setValue(params.mirror_position[2]);
    m_ui->mirror_radius->setValue(params.mirror_radius);

    m_ui->projector_position_x->setValue(params.projector_position[0]);
    m_ui->projector_position_y->setValue(params.projector_position[1]);
    m_ui->projector_position_z->setValue(params.projector_position[2]);
    m_ui->projector_euler_x->setValue(params.projector_euler[0]);
    m_ui->projector_euler_y->setValue(params.projector_euler[1]);
    m_ui->projector_euler_z->setValue(params.projector_euler[2]);
    m_ui->projector_throw_ratio->setValue(params.projector_throw_ratio);

    m_ui->source_scale->setValue(params.source_scale);
    m_ui->source_angle->setValue(params.source_angle);
    m_ui->source_mirrored->setChecked(params.source_mirrored);
}

void ConfigWidget::updateImage()
{
    if (!m_image.isNull())
    {
        sphermir_parameters_t params = getParametersFromUI();
        sphermir_set_params(engine, &params);

        m_convertedImage = QImage(m_ui->output_width->value(), m_ui->output_height->value(), QImage::Format_ARGB32);

        sphermir_convert_8uc4(engine, m_image.bits(), m_image.height(), m_image.width(), m_image.bytesPerLine(),
                m_convertedImage.bits(), m_convertedImage.height(), m_convertedImage.width(), m_convertedImage.bytesPerLine());

        m_imgLbl->setPixmap(QPixmap::fromImage(m_convertedImage));
    }
}

void ConfigWidget::loadConfig()
{
    QSettings settings;

    imgFile = settings.value("imgFile", QString()).toString();
    imgDir = settings.value("imgDir", QString()).toString();
    paramsFile = settings.value("paramsFile", QString()).toString();
    paramsDir = settings.value("paramsDir", QString()).toString();

    resize(settings.value("width", width()).toInt(), settings.value("height", height()).toInt());
    move(settings.value("x", x()).toInt(), settings.value("y", y()).toInt());
}

void ConfigWidget::saveConfig()
{
    QSettings settings;

    settings.setValue("imgFile", imgFile);
    settings.setValue("imgDir", imgDir);
    settings.setValue("paramsFile", paramsFile);
    settings.setValue("paramsDir", paramsDir);

    settings.setValue("width", width());
    settings.setValue("height", height());

    settings.setValue("x", x());
    settings.setValue("y", y());
}

void ConfigWidget::on_saveParamsBtn_clicked()
{
    paramsFile = QFileDialog::getSaveFileName(this, tr("Save Parameters as..."), paramsDir, tr("Config Files (*.xml)"));

    if (!paramsFile.isEmpty())
    {
        sphermir_error_t err = sphermir_save_params(engine, paramsFile.toAscii().data());

        if (err == SPHERMIR_FILE_OPEN_ERROR)
        {
            QMessageBox::critical(this, tr("Error"), tr("Can't create/open file %1").arg(paramsFile));
            return;
        }

        QFileInfo fileInfo(paramsFile);
        paramsDir = fileInfo.dir().absolutePath();
    }
}

void ConfigWidget::on_loadParamsBtn_clicked()
{
    paramsFile = QFileDialog::getOpenFileName(this, tr("Projection parameters"), QString(), tr("Config Files (*.xml)"));

    loadParams();
}

void ConfigWidget::on_loadBtn_clicked()
{
    imgFile = QFileDialog::getOpenFileName(this, tr("Open fisheye image"), imgDir, tr("Images (*.png *.jpg *.jpeg *.bmp)"));

    loadImage(imgFile);
}

void ConfigWidget::on_convertBtn_clicked()
{
    updateImage();

    if (m_ui->fullscreen->isChecked())
        m_imgDialog->showFullScreen();
    else
    {
        m_imgDialog->resize(m_convertedImage.size());
        m_imgDialog->showNormal();
    }
}

void ConfigWidget::closeEvent(QCloseEvent* e)
{
    saveConfig();
}

void ConfigWidget::changeEvent(QEvent* e)
{
    QWidget::changeEvent(e);
    switch (e->type())
    {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
