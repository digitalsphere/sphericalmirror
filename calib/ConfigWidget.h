#ifndef __CONFIG_WIDGET_H__
#define __CONFIG_WIDGET_H__

#include <QString>
#include <QImage>
#include <QWidget>
#include "spherical_mirror.h"

class QDialog;
class QLabel;
class QSettings;

namespace Ui
{
    class ConfigWidget;
}

class ConfigWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ConfigWidget(QWidget* parent = 0);
    ~ConfigWidget();

protected:
    void changeEvent(QEvent* e);
    void closeEvent(QCloseEvent* e);

    sphermir_parameters_t getParametersFromUI();
    void setParametersOnUI(const sphermir_parameters_t& params);

    void loadParams();
    void loadImage(const QString& fileName);

    void loadConfig();
    void saveConfig();

protected slots:
    void on_loadBtn_clicked();
    void on_convertBtn_clicked();

    void on_loadParamsBtn_clicked();
    void on_saveParamsBtn_clicked();

    void updateImage();

private:
    sphermir_engine_t* engine;

    QImage m_image;
    QImage m_convertedImage;

    QDialog* m_imgDialog;
    QLabel* m_imgLbl;

    Ui::ConfigWidget* m_ui;

    QString imgFile;
    QString imgDir;
    QString paramsFile;
    QString paramsDir;
};

#endif // __CONFIG_WIDGET_H__
