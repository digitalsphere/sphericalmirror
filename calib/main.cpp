#include <exception>
#include <iostream>
#include <QtGui>
#include "ConfigWidget.h"

int main( int argc, char** argv )
{
    try
    {
        QApplication app(argc, argv);

        QCoreApplication::setOrganizationName(QLatin1String("Digital Sphere"));
        QCoreApplication::setOrganizationDomain(QLatin1String("https://bitbucket.org/digitalsphere"));
        QCoreApplication::setApplicationName(QLatin1String("Spherical Mirror Calibration"));
        QCoreApplication::setApplicationVersion(QLatin1String("0.1.0"));

        ConfigWidget wnd;
        wnd.show();

        return app.exec();
    }
    catch (const std::exception& ex)
    {
        std::cerr << "Exception: " << ex.what() << std::endl;
    }
    catch (...)
    {
        std::cerr << "Unknown exception" << std::endl;
    }

    return -1;
}
