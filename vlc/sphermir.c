#define MODULE_STRING "spherical_mirror"

#include <vlc_common.h>
#include <vlc_plugin.h>
#include <vlc_filter.h>

#include "spherical_mirror.h"

static int Create(vlc_object_t*);
static void Destroy(vlc_object_t*);

static picture_t* Filter(filter_t*, picture_t*);
static int ConfigCallback(vlc_object_t*, char const*, vlc_value_t, vlc_value_t, void*);

#define CFG_PREFIX "spherical_mirror-"

vlc_module_begin()
    set_description( "Spherical Mirror Video Filter" )
    set_shortname( "Spherical Mirror" )
    set_help( "Convert video to spherical mirror format" )

    set_category( CAT_VIDEO )
    set_subcategory( SUBCAT_VIDEO_VFILTER )
    set_capability( "video filter2", 0 )

    add_shortcut( "spherical_mirror" )
    set_callbacks( Create, Destroy )

    add_loadfile( CFG_PREFIX "config", NULL, "Configuration file",
                  "Configuration file from calibration application", false )
vlc_module_end()

static const char* const ppsz_filter_options[] = {
    "config", NULL
};

struct filter_sys_t
{
    sphermir_engine_t* engine;
    vlc_mutex_t engine_lock;
};

static int Create(vlc_object_t* p_this)
{
    filter_t* p_filter = (filter_t*) p_this;
    filter_sys_t* p_sys;
    char* psz_string;
    sphermir_error_t err;

    /* Allocate structure */
    p_filter->p_sys = malloc(sizeof(filter_sys_t));
    if (!p_filter->p_sys)
        return VLC_ENOMEM;
    p_sys = p_filter->p_sys;

    p_sys->engine = NULL;
    err = sphermir_create(&p_sys->engine);
    if (err != SPHERMIR_SUCCESS)
    {
        msg_Err(p_filter, "Can't create sphermir engine");
        return VLC_EGENERIC;
    }

    config_ChainParse(p_filter, CFG_PREFIX, ppsz_filter_options, p_filter->p_cfg);

    psz_string = var_CreateGetStringCommand(p_filter, CFG_PREFIX "config");
    if (psz_string && *psz_string)
    {
        err = sphermir_load_params(p_sys->engine, psz_string);
        if (err != SPHERMIR_SUCCESS)
        {
            msg_Warn( p_filter, "Can't load config file" );
        }
    }
    free(psz_string);

    vlc_mutex_init(&p_sys->engine_lock);
    var_AddCallback(p_filter, CFG_PREFIX "config", ConfigCallback, p_filter);

    p_filter->pf_video_filter = Filter;

    return VLC_SUCCESS;
}

static void Destroy(vlc_object_t* p_this)
{
    filter_t* p_filter = (filter_t*) p_this;
    filter_sys_t* p_sys = p_filter->p_sys;

    var_DelCallback(p_filter, CFG_PREFIX "config", ConfigCallback, p_filter);

    vlc_mutex_destroy(&p_sys->engine_lock);

    if (p_sys->engine)
        sphermir_release(p_sys->engine);

    free(p_sys);
}

static picture_t* CopyInfoAndRelease(picture_t* p_outpic, picture_t* p_inpic)
{
    picture_CopyProperties(p_outpic, p_inpic);

    picture_Release(p_inpic);

    return p_outpic;
}

static picture_t* Filter(filter_t* p_filter, picture_t* p_pic)
{
    filter_sys_t* p_sys = p_filter->p_sys;
    picture_t* p_outpic;

    if (!p_pic)
        return NULL;

    p_outpic = filter_NewPicture(p_filter);
    if (!p_outpic)
    {
        msg_Warn(p_filter, "Can't get output picture");
        picture_Release(p_pic);
        return NULL;
    }

    vlc_mutex_lock(&p_sys->engine_lock);

    for (int i_index = 0; i_index < p_pic->i_planes; i_index++)
    {
        uint8_t* p_in;
        uint8_t* p_out;
        //uint8_t* p_in_end;

        p_in = p_pic->p[i_index].p_pixels;
        p_out = p_outpic->p[i_index].p_pixels;

        sphermir_convert_8uc1(p_sys->engine,
            p_in, p_pic->p[i_index].i_visible_lines, p_pic->p[i_index].i_visible_pitch, p_pic->p[i_index].i_pitch,
            p_out, p_pic->p[i_index].i_visible_lines, p_pic->p[i_index].i_visible_pitch, p_pic->p[i_index].i_pitch
        );
    }

    vlc_mutex_unlock(&p_sys->engine_lock);

    return CopyInfoAndRelease(p_outpic, p_pic);
}

static int ConfigCallback(vlc_object_t* p_this, char const* psz_var,
                          vlc_value_t oldval, vlc_value_t newval,
                          void* p_data)
{
    VLC_UNUSED(p_this); VLC_UNUSED(oldval);

    filter_t* p_filter = (filter_t*) p_data;
    filter_sys_t* p_sys = p_filter->p_sys;
    sphermir_error_t err;

    if (!strcmp(psz_var, CFG_PREFIX "config"))
    {
        vlc_mutex_lock(&p_sys->engine_lock);

        if (newval.psz_string && *newval.psz_string)
        {
            err = sphermir_load_params(p_sys->engine, newval.psz_string);
            if (err != SPHERMIR_SUCCESS)
            {
                msg_Warn(p_filter, "Can't load config file");
            }
        }

        vlc_mutex_unlock(&p_sys->engine_lock);
    }

    return VLC_SUCCESS;
}
